#!/bin/sh

# Linux


echo " Copyright (C) <2017>  <Alex Wolf> <github.com/flowalex999>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This script will install a Drupal 8 site running 8.3.2
    "

sleep 5s

#This moves the script into the home folder to download and expand the script
cd $HOME 
#This command downloads Drupal 8.3.2 to update the script to the newer version of drupal when released go to drupal.org and navitage to https://www.drupal.org/download and right click the download drupal 8.X.X
wget https://ftp.drupal.org/files/projects/drupal-8.4.2.tar.gz
#This unzips the drupal folder, if using a later version of drupal please change the drupal-8.X.X.tar.gz to match your version
tar xzvf drupal-8.4.2.tar.gz
#Moves the contents of the folder to the workspace directory 
mv drupal-8.4.2/* workspace/
mv drupal-8.4.2/.htaccess workspace/
#Moves to the workspace directory
cd $HOME/workspace
#Moves to the sites/default directory to create the settings.php file
cd sites/default/
cp default.settings.php settings.php
#changes the permissions of the settings.php file so that it can be written to during the install
chmod a+w settings.php
#makes the files directory and changes the permission so that files can be written during install
mkdir files
chmod a+w files/

#moves back into the workspace
cd $HOME/workspace
#installs mysql and starts it
mysql-ctl install

mysql-ctl start
mysql-ctl status
#installs phpmyadmin
phpmyadmin-ctl install



cd ~/workspace/

# Install Drupal Admin Menu Extensions
cd $HOME

wget https://ftp.drupal.org/files/projects/admin_toolbar-8.x-1.19.tar.gz
wget https://ftp.drupal.org/files/projects/toolbar_menu-8.x-2.1.tar.gz
wget https://ftp.drupal.org/files/projects/module_filter-8.x-3.0.tar.gz

tar xzvf admin_toolbar-8.x-1.19.tar.gz
tar xzvf toolbar_menu-8.x-2.1.tar.gz
tar xzvf module_filter-8.x-3.0.tar.gz

mkdir workspace/core/modules/admin_toolbar
mkdir workspace/core/modules/toolbar_menu
mkdir workspace/core/modules/module_filter

mv admin_toolbar/* workspace/core/modules/admin_toolbar/
mv toolbar_menu/* workspace/core/modules/toolbar_menu/
mv module_filter/* workspace/core/modules/module_filter/

# Install Drupal Front End and Admin Themes
#
cd $HOME

wget https://ftp.drupal.org/files/projects/nexus-8.x-1.0-beta1.tar.gz
tar xzvf nexus-8.x-1.0-beta1.tar.gz
mkdir workspace/core/themes/nexus
mv nexus/* workspace/core/themes/nexus

wget https://ftp.drupal.org/files/projects/professional_responsive_theme-8.x-1.0.tar.gz
tar xzvf professional_responsive_theme-8.x-1.0.tar.gz
mkdir workspace/core/themes/professional_responsive_theme
mv professional_responsive_theme/* workspace/core/themes/professional_responsive_theme/

wget https://ftp.drupal.org/files/projects/drupal8_zymphonies_theme-8.x-1.0.tar.gz
tar xzvf drupal8_zymphonies_theme-8.x-1.0.tar.gz
mkdir workspace/core/themes/drupal8_zymphonies_theme
mv drupal8_zymphonies_theme/* workspace/core/themes/drupal8_zymphonies_theme/

wget https://ftp.drupal.org/files/projects/adminimal_theme-8.x-1.3.tar.gz
tar xzvf adminimal_theme-8.x-1.3.tar.gz
mkdir workspace/core/themes/adminimal_theme
mv adminimal_theme/* workspace/core/themes/adminimal_theme/

#TO REMOVE THEMES
# (1) Uninstall the theme on Drupal 8 site
# (2) Run the remove command (recursive -r) to remove the theme folder and its contents
#
# EXAMPLE remove commands:
#rm -r  workspace/core/themes/nexus
#rm -r  workspace/core/themes/professional_responsive_theme
#rm -r  workspace/core/themes/drupal8_zymphonies_theme
#rm -r  workspace/core/themes/adminimal_theme
#

