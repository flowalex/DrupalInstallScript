# This repo is archived, please visit [scripts](https://gitlab.com/flowalex/scripts) for all updates
# Note: I have updated the script to 8.4.2, it works and all themes and extensions have been installed to the script
# DrupalInstallScript
A simple script to install Drupal Script, it has been tested to work for the cloud 9 platform, but it can be modified to work on your own server
There are two scripts that are on here.
The file drupal8CommerceInstallScript.sh runs the composer commands to install the drupal commerce module 
The other file drupal8installscript.sh just installs a plain drupal site.

Before running type chmod +x filename.sh  
Then run  ./filename.sh  
After running please either delete the script or type chmod -x fliename.sh
